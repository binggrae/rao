<?php


namespace frontend\controllers;


use core\entities\Certificate;
use core\entities\CertificateStatus;
use core\entities\File;
use core\helpers\CertificateHelper;
use core\helpers\FileHelper;
use yii\filters\AccessControl;
use yii\web\Controller;

class DashboardController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionIndex()
    {

        $certificates = [
            'types' => [],
            'status' => [],
        ];
        foreach (CertificateHelper::getStatusLabel() as $status => $label) {
            $count = Certificate::find()->where(['job' => $status])->count();
            if ($count) {
                $certificates['types'][$status] = [
                    'label' => $label,
                    'class' => CertificateHelper::getStatusClass($status),
                    'count' => $count
                ];
            }
        }

        foreach (CertificateStatus::map() as $id => $status) {
            $count = Certificate::find()->where(['status' => $id])->count();
            if ($count) {
                $certificates['status'][$id] = [
                    'label' => $status['label'],
                    'class' => 'label label-' . $status['class'],
                    'count' => $count
                ];
            }
        }


        $files = [
            'types' => [],
            'status' => [],
        ];
        foreach (FileHelper::getAlias() as $type => $label) {
            $count = File::find()->where(['type' => $type])->count();
            if ($count) {
                $files['types'][$type] = [
                    'label' => $label,
                    'class' => 'label label-default',
                    'count' => $count
                ];
            }
        }

        foreach (FileHelper::getStatusLabel() as $id => $label) {
            $count = File::find()->where(['status' => $id])->count();
            if ($count) {
                $files['status'][$id] = [
                    'label' => $label,
                    'class' => CertificateHelper::getStatusClass($id),
                    'count' => $count
                ];
            }
        }

        return $this->render('index', [
            'certificates' => $certificates,
            'files' => $files,
        ]);


    }



    public function actionQueue()
    {


    }

}