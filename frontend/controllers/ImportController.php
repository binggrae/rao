<?php


namespace frontend\controllers;


use core\entities\Region;
use core\entities\Structure;
use core\forms\ImportFilterForm;
use core\forms\ImportStructureForm;
use core\jobs\FilterImportJob;
use core\jobs\StructureImportJob;
use core\repositories\CertificateRepository;
use core\services\importer\structure\Importer;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\UploadedFile;

class ImportController extends Controller
{


    /** @var CertificateRepository */
    private $certificates;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionFilter()
    {
        $form = new ImportFilterForm();
        if($form->load(\Yii::$app->request->post()) && $form->file = UploadedFile::getInstance($form, 'file')) {
            if($form->save()) {
                \Yii::$app->queue->push(new FilterImportJob([
                    'file' => $form->filePath
                ]));
            }

            $this->redirect('/certificate/index');
        }

        return $this->render('filter', [
            'model' => $form
        ]);
    }


    public function actionStructure()
    {
        $form = new ImportStructureForm();
        if($form->load(\Yii::$app->request->post()) && $form->file = UploadedFile::getInstance($form, 'file')) {
            if($form->save()) {
                \Yii::$app->queue->push(new StructureImportJob([
                    'file' => $form->filePath
                ]));
            }

            $this->redirect('/certificate/index');
        }

        return $this->render('structure', [
            'model' => $form
        ]);
    }
}