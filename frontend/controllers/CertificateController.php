<?php


namespace frontend\controllers;


use codemix\excelexport\ExcelFile;
use core\entities\Certificate;
use core\entities\CertificateSearch;
use core\entities\History;
use core\forms\certificate\CertificateEditForm;
use core\forms\DownloadForm;
use core\forms\LoadForm;
use core\helpers\CertificateHelper;
use core\services\csv\Exporter;
use core\services\importer\CsvImporter;
use core\services\parser\jobs\InitJob;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class CertificateController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $model = new CertificateSearch();
        $download = new DownloadForm();

        $dataProvider = $model->search(\Yii::$app->request->queryParams);

        if ($download->load(\Yii::$app->request->post())) {
            $download->save();

            $csv = new Exporter('ochki', $download->getListTitle());

            foreach ($dataProvider->query->with('structure')->batch(100) as $certificates) {
                /** @var Certificate $certificate */
                foreach ($certificates as $certificate) {
                    $csv->addProduct($certificate);
                }
            }
            $csv->close();

            \Yii::$app->response->headers->set('Pragma', 'no-cache');
            \Yii::$app->response->sendFile(\Yii::getAlias("@common/data/result.csv"), 'Выборка от ' . date('d.m.Y H:i:s') . '.csv');
            return true;
        }

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model,
            'download' => $download
        ]);
    }


    public function actionUpdate($id)
    {
        $certificate = Certificate::find()->where(['id' => $id])->one();
        if (!$certificate) {
            throw new NotFoundHttpException('Сертификат не найден');
        }
        $form = new CertificateEditForm($certificate);

        return $this->render('update', [
                'model' => $form
            ]
        );
    }

    public function actionView($id)
    {
        $model = Certificate::find()->where(['id' => $id])->one();
        if (!$model) {
            throw new NotFoundHttpException('Страница не найдена');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => History::find()->where(['cert_id' => $model->id]),
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }
}