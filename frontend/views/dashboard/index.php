<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $certificates [] */
/* @var $files [] */


$this->title = 'Статистика';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="dashboard-index">
    <div class="row">
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-body">
                    <a class="btn btn-success" href="<?= Url::to(['/parser/start']); ?>">Обновить данные</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Лаборатории
                        <small>по статусу</small>
                    </h3>
                    <div class="box-tools pull-right">

                    </div>
                </div>
                <div class="box-body">
                    <div class="table-responsive no-padding">
                        <table class="table">
                            <?php foreach ($certificates['status'] as $certificate) : ?>
                                <tr>
                                    <td><?= $certificate['label']; ?></td>
                                    <td>
                                        <span class="label <?= $certificate['class']; ?>"><?= $certificate['count']; ?></span>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Лаборатории
                        <small>по состоянию</small>
                    </h3>
                    <div class="box-tools pull-right">

                    </div>
                </div>
                <div class="box-body">
                    <div class="table-responsive no-padding">
                        <table class="table">
                            <?php foreach ($certificates['types'] as $certificate) : ?>
                                <tr>
                                    <td><?= $certificate['label']; ?></td>
                                    <td>
                                        <span class="label <?= $certificate['class']; ?>"><?= $certificate['count']; ?></span>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Файлы
                        <small>по статусу</small>
                    </h3>
                    <div class="box-tools pull-right">

                    </div>
                </div>
                <div class="box-body">
                    <div class="table-responsive no-padding">
                        <table class="table">
                            <?php foreach ($files['status'] as $file) : ?>
                                <tr>
                                    <td><?= $file['label']; ?></td>
                                    <td>
                                        <span class="label <?= $file['class']; ?>"><?= $file['count']; ?></span>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Файлы
                        <small>по состоянию</small>
                    </h3>
                    <div class="box-tools pull-right">

                    </div>
                </div>
                <div class="box-body">
                    <div class="table-responsive no-padding">
                        <table class="table">
                            <?php foreach ($files['types'] as $file) : ?>
                                <tr>
                                    <td><?= $file['label']; ?></td>
                                    <td>
                                        <span class="label <?= $file['class']; ?>"><?= $file['count']; ?></span>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

