<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model core\entities\Area */

$this->title = 'Редактирование объекта исследования: ' .$model->title;
$this->params['breadcrumbs'][] = ['label' => 'Объекты исследования', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирования';
?>
<div class="area-update">

    <?php $form = ActiveForm::begin(); ?>
    <div class="box box-success">
        <div class="box-header">
            <div class="box-tools pull-right">
                <?= Html::submitButton('<i class="fa fa-plus text-blue"></i>', [
                    'class' => 'btn btn-save btn-sm',
                    'title' => 'Сохранить'
                ]) ?>
            </div>
        </div>
        <div class="box-body">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="box-footer">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
