<?php

use core\entities\Area;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Объекты исследования';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="area-index">

    <div class="box box-success">
        <div class="box-header">
            <div class="box-tools pull-right">
                <?= Html::a('<i class="fa fa-plus text-blue"></i>', ['create'], [
                    'class' => 'btn btn-default btn-sm',
                    'title' => 'Создать'
                ]) ?>
            </div>
        </div>
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    'alias',
                    [
                        'attribute' => 'title',
                        'format' => 'raw',
                        'value' => function (Area $area) {
                            return Html::a($area->title, ['/area/update', 'id' => $area->id]);
                        }
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{delete}'
                    ],
                ],
            ]); ?>
        </div>

        <div class="box-footer">
            <?= Html::a('Создать', ['create'], [
                'class' => 'btn btn-primary',
            ]) ?>
        </div>
    </div>


</div>
