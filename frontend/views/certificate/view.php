<?php

use core\entities\Certificate;
use core\entities\History;
use core\helpers\FileHelper;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model Certificate */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->uid;
$this->params['breadcrumbs'][] = ['label' => 'Список АО', 'url' => ['/certificate/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-6">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Общая информация</h3>
            </div>
            <div class="box-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'attribute' => 'uid',
                            'format' => 'raw',
                            'value' => Html::a($model->uid, $model->url, ['target' => '_blank'])
                        ],

                        'updated_at:datetime',
                        'status:raw',
                        [
                            'attribute' => 'regions',
                            'format' => 'raw',
                            'value' => function () use ($model) {
                                $result = '';
                                foreach ($model->regions as $region) {
                                    $result .= Html::tag('span', $region->title, [
                                            'class' => 'label label-default'
                                        ]) . ' ';
                                }
                                return $result;
                            }
                        ],
                        [
                            'attribute' => 'areas',
                            'format' => 'raw',
                            'value' => function () use ($model) {
                                $result = '';
                                foreach ($model->areas as $area) {
                                    $result .= Html::tag('span', $area->title, [
                                            'class' => 'label label-default'
                                        ]) . ' ';
                                }
                                return $result;
                            }
                        ]
                    ],
                ]) ?>
            </div>
        </div>
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Список файлов</h3>
            </div>
            <div class="box-body">
                <table class="table">
                    <?php foreach ($model->files as $file) : ?>
                        <tr>
                            <td><?= FileHelper::getAlias($file->type); ?></td>
                            <td><?= $file->name; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">История изменений</h3>
            </div>
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'attribute' => 'type',
                            'format' => 'raw',
                            'value' => function (History $history) {
                                return $history->name;
                            }
                        ],
                        'created_at:datetime',
                        [
                            'attribute' => 'data',
                            'format' => 'raw',
                            'value' => function (History $history) {
                                return $history->value;
                            }
                        ],

                    ],
                ]); ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Данные реестра</h3>
            </div>
            <div class="box-body table-responsive">
                <?php if ($model->structure) : ?>
                    <?= DetailView::widget([
                        'model' => $model->structure,
                        'attributes' => [
                            'type_al:ntext',
                            'type_ap:ntext',
                            'opf_ap:ntext',
                            'full_name_ap:ntext',
                            'short_name_ap:ntext',
                            'trade_name_ap:ntext',
                            'fio_ruk_ap:ntext',
                            'dol_ruk_ap:ntext',
                            'address_ap:ntext',
                            'address_delo_ap:ntext',
                            'phone_ap:ntext',
                            'email_ap:ntext',
                            'ogrn:ntext',
                            'inn_ap:ntext',
                            'full_name_al:ntext',
                            'short_name_al:ntext',
                            'fio_ruk_al:ntext',
                            'address_al:ntext',
                            'phone_al:ntext',
                            'email_al:ntext',
                            'website_al:ntext',
                            'lab_al:ntext',
                            'own_lab_al:ntext',
                            'expert_al:ntext',
                            'fio_rab_al:ntext',
                            'obl_akk_al:ntext',
                            'status:ntext',
                            'reg_number:ntext',
                            'on_reg_dat:ntext',
                            'off_att_akk_dat:ntext',
                            'akkred_order_num:ntext',
                            'akkred_order_dat:ntext',
                            'akkred_exp:ntext',
                            'izm_att_akk:ntext',
                            'ts:ntext',
                            'gk:ntext',
                        ],
                    ]) ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
