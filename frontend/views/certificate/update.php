<?php

use core\forms\certificate\CertificateEditForm;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model CertificateEditForm */

$this->title = 'Редактирование сертификата ' . $model->certificate->uid;
$this->params['breadcrumbs'][] = ['label' => 'Список АО', 'url' => ['/certificate/index']];
$this->params['breadcrumbs'][] = ['label' => $model->certificate->uid, 'url' => ['/certificate/view', ['id' => $model->certificate->id]]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>

<div class="box box-primary">
    <?php $form = Html::beginForm([
        'method' => 'post'
    ]); ?>


    <?php Html::endForm(); ?>
</div>
