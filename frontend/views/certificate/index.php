<?php

use core\entities\Area;
use core\entities\Certificate;
use core\entities\CertificateSearch;
use core\entities\CertificateStatus;
use core\entities\Region;
use core\entities\Structure;
use core\forms\DownloadForm;
use core\helpers\CertificateHelper;
use core\helpers\HistoryHelper;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $download DownloadForm */
/* @var $model CertificateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs("
$('.jsFilterCheck').on('click', function() {
  var isAll = $(this).data('all');
  $.each($('#downloadform-list input[type=\"checkbox\"]'), function(i, el) {
      $(el).attr(\"checked\", !isAll);
    });
  $(this).data('all', !isAll);
});


")

?>
<div class="certificate-index">
    <div class="row">
        <div class="col-md-12">
            <?php $form = ActiveForm::begin([
                'action' => 'index',
                'method' => 'get',
            ]); ?>
            <div class="box box-success collapsed-box">
                <div class="box-header">
                    <h3 class="box-title">
                        Поиск
                    </h3>
                    <div class="box-tools pull-right">
                        <?= Html::submitButton('<i class="fa fa-search text-blue"></i>', [
                            'class' => 'btn btn-default btn-sm',
                            'title' => 'Найти'
                        ]) ?>
                        <?= Html::a('<i class="fa fa-close text-red"></i>', ['index'], [
                            'class' => 'btn btn-default btn-sm',
                            'title' => 'Сбросить'
                        ]) ?>
                        <button type="button" class="btn btn-default btn-sm" data-widget="collapse">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="history-search">
                        <?php $form = ActiveForm::begin([
                            'action' => ['index'],
                            'method' => 'get',
                        ]); ?>

                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'uid')->textInput() ?>
                                <?= $form->field($model, 'status')->checkboxList(CertificateStatus::getList()) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'regions')->dropDownList(Region::getList(),[
                                    'multiple' => 'multiple'
                                ]) ?>
                                <?= $form->field($model, 'areas')->dropDownList(Area::getList(), [
                                    'multiple' => 'multiple'
                                ]) ?>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'type')->checkboxList(HistoryHelper::getLabels()) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'date_start')->widget(DatePicker::className(), [
                                    'language' => 'ru',
                                    'dateFormat' => 'dd.MM.yyyy',
                                    'options' => [
                                        'class' => 'form-control'
                                    ]
                                ]) ?>
                                <?= $form->field($model, 'date_end')->widget(DatePicker::className(), [
                                    'language' => 'ru',
                                    'dateFormat' => 'dd.MM.yyyy',
                                    'options' => [
                                        'class' => 'form-control'
                                    ]
                                ]) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>

                <div class="box-footer">
                    <?= Html::submitButton('Найти', [
                        'class' => 'btn btn-primary',
                        'title' => 'Найти'
                    ]) ?>
                    <?= Html::a('Сбросить', ['index'], [
                        'class' => 'btn btn-default',
                        'title' => 'Сбросить'
                    ]) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-12">
            <?php $form = ActiveForm::begin([
                'method' => 'post',
            ]); ?>
            <div class="box box-success collapsed-box">
                <div class="box-header">
                    <h3 class="box-title">
                        Экспорт
                    </h3>
                    <div class="box-tools pull-right">
                        <?= Html::submitButton('<i class="fa fa-download text-blue"></i>', [
                            'class' => 'btn btn-default btn-sm',
                            'title' => 'Скачать'
                        ]) ?>
                        <button type="button" class="btn btn-default btn-sm jsFilterCheck">
                            <i class="fa fa-check"></i>
                        </button>
                        <button type="button" class="btn btn-default btn-sm" data-widget="collapse">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <?= $form->field($download, 'list', [

                    ])->checkboxList(Structure::getAttributeList(), [
                        'multiple' => 'multiple',
                    ]); ?>
                </div>
                <div class="box-footer">
                    <div class="form-group">
                        <?= Html::submitButton('Скачать', ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>

            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <div class="box box-primary">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'id',
                        'format' => 'raw',
                        'value' => function (Certificate $certificate) {
                            return $certificate->link ?
                                Html::a($certificate->id, $certificate->url, ['target' => '_blank'])
                                : $certificate->id;
                        }
                    ],
                    [
                        'attribute' => 'uid',
                        'format' => 'raw',
                        'value' => function (Certificate $certificate) {
                            $span = Html::tag('div', $certificate->uid, [
                                'class' => 'btn btn-xs btn-' . $certificate->status->class,
                                'title' => $certificate->status->label,
                            ]);
                            return Html::a($span, ['/certificate/view', 'id' => $certificate->id]);
                        }
                    ],
                    [
                        'attribute' => 'updated_at',
                        'format' => 'raw',
                        'value' => function (Certificate $certificate) {
                            return Yii::$app->formatter->asDatetime($certificate->updated_at);
                        }
                    ],
                    [
                        'attribute' => 'region_id',
                        'format' => 'raw',
                        'value' => function (Certificate $certificate) use ($model) {
                            $result = '';
                            foreach ($certificate->regions as $area){
                                $result .= Html::tag('span', $area->title, [
                                        'class' => 'label label-' . ($model->hasSearchRegion($area->id) ? 'success' : 'default')
                                    ]) . ' ';
                            }
                            return $result;
                        }
                    ],
                    [
                        'header' => 'Объекты исследования',
                        'format' => 'raw',
                        'value' => function (Certificate $certificate) use ($model) {
                            $result = '';
                            foreach ($certificate->areas as $area){
                                $result .= Html::tag('span', $area->title, [
                                    'class' => 'label label-' . ($model->hasSearchArea($area->id) ? 'success' : 'default')
                                ]) . ' ';
                            }
                            return $result;
                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
