<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $certificates [] */
/* @var $files [] */


$this->title = 'Статистика';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-12">
            <a class="btn btn-success" href="<?=Url::to(['/parser/start']);?>">Запустить</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <h4>Лаборатории
                <small>по статусу</small>
            </h4>
            <table class="table">
                <?php foreach ($certificates['status'] as $certificate) : ?>
                    <tr>
                        <td><?= $certificate['label']; ?></td>
                        <td>
                            <span class="label <?= $certificate['class']; ?>"><?= $certificate['count']; ?></span>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>

            <hr>

            <h4>Лаборатории
                <small>по состоянию</small>
            </h4>
            <table class="table">
                <?php foreach ($certificates['types'] as $certificate) : ?>
                    <tr>
                        <td><?= $certificate['label']; ?></td>
                        <td>
                            <span class="label <?= $certificate['class']; ?>"><?= $certificate['count']; ?></span>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>

        <div class="col-md-6">
            <h4>Файлы
                <small>по типу</small>
            </h4>
            <table class="table">
                <?php foreach ($files['types'] as $file) : ?>
                    <tr>
                        <td><?= $file['label']; ?></td>
                        <td>
                            <span><?= $file['count']; ?></span>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>

            <hr>

            <h4>Файлы
                <small>по состоянию</small>
            </h4>
            <table class="table">
                <?php foreach ($files['status'] as $file) : ?>
                    <tr>
                        <td><?= $file['label']; ?></td>
                        <td>
                            <span class="label <?= $file['class']; ?>"><?= $file['count']; ?></span>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>


</div>
