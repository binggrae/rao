<aside class="main-sidebar">

    <section class="sidebar">
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Лаборатории', 'options' => ['class' => 'header']],
                    ['label' => 'Список', 'icon' => 'list', 'url' => ['/certificate/index']],
                    ['label' => 'Файлы', 'icon' => 'file-o', 'url' => ['/file/index']],

                    ['label' => 'Импорт', 'options' => ['class' => 'header']],
                    ['label' => 'Фильтры', 'icon' => 'filter', 'url' => ['/import/filter']],
                    ['label' => 'Данные АО', 'icon' => 'database', 'url' => ['/import/structure']],


                    ['label' => 'Настройки', 'options' => ['class' => 'header']],

                    ['label' => 'Панель управления', 'icon' => 'dashboard', 'url' => ['/dashboard']],
                    ['label' => 'Объекты исследования', 'icon' => 'sitemap', 'url' => ['/area']],
                ],
            ]
        ) ?>
    </section>
</aside>

