<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \core\forms\ImportFilterForm */

$this->title = 'Импорт фильтров';
$this->params['breadcrumbs'][] = ['label' => 'Список АО', 'url' => ['/certificate/index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="certificate-load">
    <?php $form = ActiveForm::begin(['options' => [
        'enctype' => 'multipart/form-data'
    ]]); ?>

    <?= $form->field($model, 'file')->fileInput(); ?>

    <div class="form-group">
        <?= Html::submitButton('Загрузить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
