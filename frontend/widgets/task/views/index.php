<?php

/** @var int $count */
/** @var int $waiting */
/** @var int $delayed */
/** @var int $reserved */
/** @var int $done */

?>
<li class="dropdown notifications-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-flag-o"></i>
        <span class="label label-success"><?= $count; ?></span>
    </a>
    <ul class="dropdown-menu">
        <li class="header"><?= Yii::t('app', 'В очереди {n, plural, =0{нет заданий} =1{одно задание} one{# задание} few{# задания} many{# заданий} other{# заданий}}', [
                'n' => $count
            ]); ?></li>
        <li>
            <ul class="menu">
                <li>
                    <a>
                        <span>В очереди</span>
                        <span class="label label-success pull-right"><?=$waiting;?></span>
                    </a>
                </li>
                <li>
                    <a>
                        <span>Отложенный запуск</span>
                        <span class="label label-primary pull-right"><?=$delayed;?></span>
                    </a>
                </li>
                <li>
                    <a>
                        <span>В работе</span>
                        <span class="label label-warning pull-right"><?=$reserved;?></span>
                    </a>
                </li>
                <li>
                    <a>
                        <span>Ошибка</span>
                        <span class="label label-danger pull-right"><?=$done;?></span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</li>