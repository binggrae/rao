<?php


namespace frontend\widgets\task;


use yii\base\Widget;
use yii\db\Query;

class TaskList extends Widget
{

    private $count;
    private $waiting;
    private $delayed;
    private $reserved;
    private $done;

    public function init()
    {
        $this->count = $this->getAll()->count();

        $this->waiting = $this->getWaiting()->count();
        $this->delayed = $this->getDelayed()->count();
        $this->reserved = $this->getReserved()->count();
        $this->done = $this->getDone()->count();

        parent::init();
    }


    public function run()
    {

        return $this->render('index', [
            'count' => $this->count,
            'waiting' => $this->waiting,
            'delayed' => $this->delayed,
            'reserved' => $this->reserved,
            'done' => $this->done,
        ]);

    }


    /**
     * @return Query
     */
    protected function getAll()
    {
        return (new Query())
            ->from('queue')
            ->andWhere(['channel' => 'default']);
    }

    /**
     * @return Query
     */
    protected function getWaiting()
    {
        return (new Query())
            ->from('queue')
            ->andWhere(['channel' => 'default'])
            ->andWhere(['reserved_at' => null])
            ->andWhere(['delay' => 0]);
    }

    /**
     * @return Query
     */
    protected function getDelayed()
    {
        return (new Query())
            ->from('queue')
            ->andWhere(['channel' => 'default'])
            ->andWhere(['reserved_at' => null])
            ->andWhere(['>', 'delay', 0]);
    }

    /**
     * @return Query
     */
    protected function getReserved()
    {
        return (new Query())
            ->from('queue')
            ->andWhere(['channel' => 'default'])
            ->andWhere('[[reserved_at]] is not null')
            ->andWhere(['done_at' => null]);
    }

    /**
     * @return Query
     */
    protected function getDone()
    {
        return (new Query())
            ->from('queue')
            ->andWhere(['channel' => 'default'])
            ->andWhere('[[done_at]] is not null');
    }


}