<?php

namespace core\entities;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "objects".
 *
 * @property int $id
 * @property string $alias
 * @property string $title
 *
 * @property CertificateArea[] $certificateObjects
 */
class Area extends \yii\db\ActiveRecord
{

    public static function create($alias, $title = '')
    {
        $area = new self();
        $area->alias = $alias;
        $area->title = $title ?: $alias;

        return $area;
    }


    public static function tableName()
    {
        return 'objects';
    }


    public function rules()
    {
        return [
            [['alias', 'title'], 'required'],
            [['alias', 'title'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Псевдоним',
            'title' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificateObjects()
    {
        return $this->hasMany(CertificateArea::className(), ['object_id' => 'id']);
    }

    public static function getList()
    {
        return ArrayHelper::merge([null => '-- Любой --'], ArrayHelper::map(self::find()->orderBy('title')->all(), 'id', 'title'));
    }
}
