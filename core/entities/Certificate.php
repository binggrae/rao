<?php

namespace core\entities;

use core\entities\queries\CertificateQuery;
use core\helpers\CertificateHelper;
use core\helpers\FileHelper;
use core\services\csv\ExporterInterface;
use core\services\parser\elements\ScopeFile;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * This is the model class for table "certificates".
 *
 * @property int $id
 * @property string $uid
 * @property int $created_at
 * @property int $updated_at
 * @property string $link
 * @property int|CertificateStatus $status
 * @property int $job
 * @property string $url
 *
 * @property File[] $files
 * @property mixed $xlsUpdated
 * @property mixed $xlsCreated
 * @property mixed $xlsStatus
 * @property Region[] $regions
 * @property CertificateArea[] $certificateAreas
 * @property Area[] $areas
 * @property Structure $structure
 * @property History[] $histories
 */
class Certificate extends ActiveRecord implements ExporterInterface
{
    public $files_ids = [];

    private $last_update_dat;
    private $url;
    private $status_label;
    private $region_name;
    private $areas_name;

    public static function create($uid, $date)
    {
        $model = Certificate::find()->byUid($uid)->one(); 
        if (!$model) {
            $model = new self(); 
			$model->uid = $uid;
			$model->job = CertificateHelper::STATUS_NEW;
			$model->created_at = strtotime($date) > 0 ? strtotime($date) : 0;
			$model->updated_at = time();
			$model->save();
        }


        return $model;
    }

    public function changeLink($link)
    {
        $this->link = $link;
        $this->updated_at = time();
    }

    public function changeStatus($status)
    {
        if(is_null($this->status) || $this->status->id !== $status->id) {
            $this->status = $status;
            $this->updated_at = time();

            $history = StatusHistory::create($this->id, $this->status->id);
            $history->save();
        }
    }


    /**
     * @param ScopeFile[] $files
     */
    public function addFiles($files)
    {
        foreach ($files as $file) {
            $update = true;
            $model = File::find()->where(['uid' => $file->uid])->one();
            if (!$model) {
                $update = false;
                $model = File::create($this->id, $file);
            }

            if ($model->name != $file->name) {
                $model->changeName($file->name);
            }
            $model->save();

            if ($model->status == FileHelper::STATUS_WAIT) {
                $this->files_ids[] = $model->id;

                if ($update) {
                    $history = UpdateFileHistory::create($this->id, $model->id);
                } else {
                    $history = AddFileHistory::create($this->id, $model->id);
                }
                $history->save();
            }
        }
    }

    public function loadStructure($data)
    {
        $structure = $this->structure ?: new Structure();
        $structure->setAttributes($data);

        return $structure;
    }

    public static function getChunks($job)
    {
        do {
            $certificates = (new Query())
                ->select('id')
                ->from('certificates')
                ->andWhere(['job' => $job])
                ->andWhere(['or', ["!=", "status", CertificateStatus::STATUS_DELETE], ["!=", "status", CertificateStatus::STATUS_ARCHIVE]])
                ->limit(10)
                ->all();

            if(!count($certificates)) {
                continue;
            }

            $ids = [];
            foreach ($certificates as $certificate) {
                $ids[] = $certificate['id'];
            }    

            \Yii::$app->db->createCommand('UPDATE certificates SET job = :job WHERE id IN ('. implode(',', $ids).')')
                ->bindValue(':job', CertificateHelper::STATUS_WAIT)
                ->execute();    

            yield $ids;
        

        } while(count($certificates));
    }


    public static function tableName()
    {
        return 'certificates';
    }

    public static function find()
    {
        return (new CertificateQuery(get_called_class()));
    }

    public function rules()
    {
        return [
            [['uid', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at', 'job'], 'integer'],
            [['status'], 'safe'],
            [['uid', 'link'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'region_id' => 'Регион',
            'uid' => 'Регистрационный номер',
            'created_at' => 'Дата регистрации',
            'updated_at' => 'Обновление',
            'link' => 'Ссылка',
            'status' => 'Статус',
            'areas' => 'Объекты исследования',
            'job' => 'Состояние',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => SaveRelationsBehavior::className(),
                'relations' => ['regions', 'areas', 'structure'],
            ],
        ];
    }

    public function getXlsCreated()
    {
        return \Yii::$app->formatter->asDate($this->created_at, 'short');
    }

    public function getLast_update_dat()
    {
        return \Yii::$app->formatter->asDatetime($this->updated_at, 'short');
    }

    public function getStatus_label()
    {
        return $this->status->label;
    }

    public function getRegion_name()
    {
        $result = '';
        foreach ($this->regions as $region) {
            $result .= $region->alias;
        }
        return $result;
    }

    public function getAreas_name()
    {
        $result = '';
        foreach ($this->areas as $area) {
            $result .= $area->alias;
        }
        return $result;
    }

    public function beforeSave($insert)
    {
        if (!is_null($this->status)) {
            $this->status = $this->status->id;
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (!is_null($this->status)) {
            $this->status = CertificateStatus::createById($this->status);
        }

        parent::afterSave($insert, $changedAttributes);
    }

    public function afterFind()
    {
        if (!is_null($this->status)) {
            $this->status = CertificateStatus::createById($this->status);
        }
        parent::afterFind();

    }

    public function getUrl()
    {
        return 'http://188.254.71.82/rao_rf_pub/?show=view&id_object=' . $this->link;
    }

    public function getRegions()
    {
        return $this->hasMany(Region::className(), ['id' => 'region_id'])
            ->viaTable(CertificateRegion::tableName(), ['certificate_id' => 'id']);
    }

    public function getCertificateAreas()
    {
        return $this->hasMany(CertificateArea::className(), ['certificate_id' => 'id']);
    }

    public function getAreas()
    {
        return $this->hasMany(Area::className(), ['id' => 'object_id'])
            ->viaTable(CertificateArea::tableName(), ['certificate_id' => 'id']);
    }

    public function getStructure()
    {
        return $this->hasOne(Structure::className(), ['certificate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['cert_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistories()
    {
        return $this->hasMany(History::className(), ['cert_id' => 'id']);
    }

    public function print($titles)
    {
        $result = [];
        foreach ($titles as $title) {
            $method = 'get'.ucfirst($title);
            $result[$title] = property_exists($this, $title) ? $this->{$method}() : $this->structure->{$title};
        }

        return $result;

    }
}
