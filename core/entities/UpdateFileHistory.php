<?php


namespace core\entities;


class UpdateFileHistory extends History
{
    const TYPE = 'update_file';

    public function getValue()
    {
        return 'Изменение статуса';
    }

}