<?php

namespace core\entities;

use Yii;

/**
 * This is the model class for table "certificate_objects".
 *
 * @property int $certificate_id
 * @property int $object_id
 *
 * @property Certificate $certificate
 * @property Object $object
 */
class CertificateArea extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'certificate_objects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['certificate_id', 'object_id'], 'integer'],
            [['certificate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Certificate::className(), 'targetAttribute' => ['certificate_id' => 'id']],
            [['object_id'], 'exist', 'skipOnError' => true, 'targetClass' => Area::className(), 'targetAttribute' => ['object_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'certificate_id' => 'Certificate ID',
            'object_id' => 'Object ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificate()
    {
        return $this->hasOne(Certificate::className(), ['id' => 'certificate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(Area::className(), ['id' => 'object_id']);
    }
}
