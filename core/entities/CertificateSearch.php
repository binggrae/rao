<?php


namespace core\entities;


use core\helpers\CertificateHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class CertificateSearch extends Model
{

    public $status;

    public $regions = [];

    public $areas = [];

    public $type;

    public $date_start;

    public $date_end;

    public $uid;

    public function init()
    {
        $this->status = [
            CertificateStatus::STATUS_ACTIVE,
            CertificateStatus::STATUS_PAUSE,
            CertificateStatus::STATUS_PART_PAUSE
        ];
        parent::init();
    }

    public function rules()
    {
        return [
            [['uid'], 'string'],
            [['areas', 'regions'], 'each', 'rule' => ['integer']],
            [['status', 'type', 'date_start', 'date_end'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'uid' => 'Номер аттестата',
            'regions' => 'Регион',
            'areas' => 'Объекты исследования',
            'status' => 'Статус',
            'type' => 'Тип изменения',
            'date_start' => 'Дата ОТ',
            'date_end' => 'Дата ДО',
        ];
    }

    public function search($params)
    {
        $c = Certificate::tableName();
        $h = History::tableName();
        $ca = CertificateArea::tableName();
        $cr = CertificateRegion::tableName();

        $query = Certificate::find();
        $query->with('regions');
        $query->with('areas');
        $query->select("{$c}.*");
        $query->from("{$c}");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'status' => $this->status,
        ]);
        $query->andFilterWhere(['like', 'uid', $this->uid]);

        if ($this->areas) {
            $null_area = false;
            foreach ($this->areas as $area) {
                if ($area == '') {
                    $null_area = true;
                }
            }
            if (!$null_area) {
                $query->innerJoin($ca, "{$ca}.certificate_id = {$c}.id");
                $query->andWhere(["{$ca}.object_id" => $this->areas]);
            }
        }

        if ($this->regions) {
            $null_region = false;
            foreach ($this->regions as $region) {
                if ($region == '') {
                    $null_region = true;
                }
            }
            if (!$null_region) {
                $query->innerJoin($cr, "{$cr}.certificate_id = {$c}.id");
                $query->andWhere(["{$cr}.region_id" => $this->regions]);
            }
        }



        if ($this->type) {
            $query->innerJoin($h, "{$h}.cert_id = {$c}.id");
            $query->andWhere(["{$h}.type" => $this->type]);

            if ($this->date_start) {
                $query->andWhere(['>=', "{$h}.created_at", strtotime($this->date_start)]);
            }

            if ($this->date_end) {
                $query->andWhere(['<=', "{$h}.created_at", strtotime($this->date_end . ' 23:59:59')]);
            }

            $query->groupBy("{$c}.id");
            $query->having("COUNT(DISTINCT {$h}.type) >= " . count($this->type));
        }

        return $dataProvider;
    }

    public function hasSearchArea($id)
    {
        if($this->areas ) {
            foreach ($this->areas as $area) {
                if ($area == $id) {
                    return true;
                }
            }
        }

        return false;
    }

    public function hasSearchRegion($id)
    {
        if($this->areas ) {
            foreach ($this->areas as $area) {
                if ($area == $id) {
                    return true;
                }
            }
        }

        return false;
    }
}