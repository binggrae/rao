<?php

namespace core\entities;

use Yii;

/**
 * This is the model class for table "structures".
 *
 * @property int $id
 * @property int $certificate_id
 * @property string $type_al
 * @property string $type_ap
 * @property string $opf_ap
 * @property string $full_name_ap
 * @property string $short_name_ap
 * @property string $trade_name_ap
 * @property string $fio_ruk_ap
 * @property string $dol_ruk_ap
 * @property string $address_ap
 * @property string $address_delo_ap
 * @property string $phone_ap
 * @property string $email_ap
 * @property string $ogrn
 * @property string $inn_ap
 * @property string $full_name_al
 * @property string $short_name_al
 * @property string $fio_ruk_al
 * @property string $address_al
 * @property string $phone_al
 * @property string $email_al
 * @property string $website_al
 * @property string $lab_al
 * @property string $own_lab_al
 * @property string $expert_al
 * @property string $fio_rab_al
 * @property string $obl_akk_al
 * @property string $status
 * @property string $reg_number
 * @property string $on_reg_dat
 * @property string $off_att_akk_dat
 * @property string $akkred_order_num
 * @property string $akkred_order_dat
 * @property string $akkred_exp
 * @property string $izm_att_akk
 * @property string $ts
 * @property string $gk
 *
 * @property Certificate $certificate
 */
class Structure extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'structures';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_al', 'type_ap', 'opf_ap', 'full_name_ap', 'short_name_ap', 'trade_name_ap', 'fio_ruk_ap', 'dol_ruk_ap', 'address_ap', 'address_delo_ap', 'phone_ap', 'email_ap', 'ogrn', 'inn_ap', 'full_name_al', 'short_name_al', 'fio_ruk_al', 'address_al', 'phone_al', 'email_al', 'website_al', 'lab_al', 'own_lab_al', 'expert_al', 'fio_rab_al', 'obl_akk_al', 'status', 'reg_number', 'on_reg_dat', 'off_att_akk_dat', 'akkred_order_num', 'akkred_order_dat', 'akkred_exp', 'izm_att_akk', 'ts', 'gk'], 'string'],
            [['certificate_id'], 'exist', 'skipOnError' => true, 'targetClass' => Certificate::className(), 'targetAttribute' => ['certificate_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'type_al' => 'Тип аккредитованного лица',
            'type_ap' => 'Тип заявителя',
            'opf_ap' => 'Организационно-правовая форма заявителя',
            'full_name_ap' => 'Полное наименование/ФИО заявителя',
            'short_name_ap' => 'Сокращенное наименование заявителя',
            'trade_name_ap' => 'Фирменное наименование заявителя',
            'fio_ruk_ap' => 'ФИО руководителя заявителя',
            'dol_ruk_ap' => 'Должность руководителя заявителя',
            'address_ap' => 'Адрес заявителя',
            'address_delo_ap' => 'Адреса мест осуществления деятельности в области аккредитации',
            'phone_ap' => 'Номер телефона заявителя',
            'email_ap' => 'Адрес электронной почты заявителя',
            'ogrn' => 'ОГРН/ОГРН ИП заявителя',
            'inn_ap' => 'ИНН заявителя',
            'full_name_al' => 'Полное наименование аккредитованного лица',
            'short_name_al' =>'Сокращенное наименование аккредитованного лица',
            'fio_ruk_al' => 'ФИО руководителя аккредитованного лица',
            'address_al' => 'Адрес аккредитованного лица',
            'phone_al' => 'Номер телефона аккредитованного лица',
            'email_al' => 'Адрес электронной почты аккредитованного лица',
            'website_al' => 'Адрес web-сайта в сети Интернет аккредитованного лица',
            'lab_al' => 'Лаборатории, с которыми заключены договора на проведение испытаний для целей сертификации',
            'own_lab_al' => 'Наличие собственной испытательной базы',
            'expert_al' => 'Сведения об экспертах',
            'fio_rab_al' => 'ФИО работника',
            'obl_akk_al' => 'Область аккредитации',
            'status' => 'Статус',
            'reg_number' => 'Номер аттестата',
            'on_reg_dat' => 'Дата включения аккредитованного лица в реестр',
            'off_att_akk_dat' => 'Срок действия аттестата аккредитации',
            'akkred_order_num' => 'Номер приказа об аккредитации',
            'akkred_order_dat' => 'Дата приказа об аккредитации',
            'akkred_exp' => 'Сведения об экспертных организациях и экспертах',
            'izm_att_akk' => 'Дата изменения сертификата',
            'ts' => 'Национальная часть Единого реестра Таможенного союза',
            'gk' => 'Государственный контроль',
        ];
    }

    public static function getAttributeList()
    {
        return  (new self())->attributeLabels();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificate()
    {
        return $this->hasOne(Certificate::className(), ['id' => 'certificate_id']);
    }
}
