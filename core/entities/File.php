<?php

namespace core\entities;

use core\helpers\FileHelper;
use core\services\parser\elements\ScopeFile;
use Yii;

/**
 * This is the model class for table "file".
 *
 * @property int $id
 * @property int $cert_id
 * @property string $uid
 * @property string $name
 * @property string $extension
 * @property string $type
 * @property int $created_at
 * @property int $size
 * @property int $status
 * @property string $gpath
 * @property string $url
 * @property string $rootPath
 *
 * @property Certificate $certificate
 */
class File extends \yii\db\ActiveRecord
{
    private $_path;

    public static function create($cert_id, ScopeFile $scopeFile)
    {
        $file = new self();
        $file->cert_id = $cert_id;
        $file->name = $scopeFile->name;
        $file->uid = $scopeFile->uid;
        $file->type = $scopeFile->type;
        $file->created_at = time();
        $file->status = FileHelper::STATUS_WAIT;

        return $file;
    }

    public function changeName($name)
    {
        $this->name = $name;
        $this->status = FileHelper::STATUS_WAIT;
    }

    public function getRootPath()
    {
        if (!$this->_path) {
            $path = Yii::getAlias('@common/data/');

            $segments = [
                substr($this->uid, 0, 2),
                substr($this->uid, 2, 2),
                $this->type
            ];

            foreach ($segments as $segment) {
                $path .= $segment . '/';
                if (!file_exists($path)) {
                    mkdir($path);
                }
            }

            $this->_path = $path . $this->uid;
        }

        return $this->_path;
    }

    public function clearTmp()
    {
        if (file_exists($this->rootPath)) {
            unlink($this->rootPath);
        }
    }

    public function getUrl()
    {
        return "http://188.254.71.82/rao_rf_pub/?ajax=files&action=get_file&file_id={$this->uid}&getFile=1";
    }

    public static function tableName()
    {
        return 'file';
    }


    public function rules()
    {
        return [
            [['cert_id', 'created_at', 'size', 'status'], 'integer'],
            [['uid', 'name', 'extension', 'gpath'], 'string', 'max' => 255],
            [['cert_id'], 'exist', 'skipOnError' => true, 'targetClass' => Certificate::className(), 'targetAttribute' => ['cert_id' => 'id']],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cert_id' => 'Cert ID',
            'uid' => 'Uid',
            'name' => 'Name',
            'extension' => 'Extension',
            'created_at' => 'Created At',
            'size' => 'Size',
            'status' => 'Status',
            'gpath' => 'Gpath',
        ];
    }

    public function beforeDelete()
    {
        $histories = AddFileHistory::find()->where(['data' => $this->id])->all();
        foreach ($histories as $history) {
            $history->delete();
        }

        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificate()
    {
        return $this->hasOne(Certificate::className(), ['id' => 'cert_id']);
    }
}
