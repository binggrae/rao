<?php


namespace core\entities;


use yii\base\NotSupportedException;
use yii\helpers\Html;

class CertificateStatus
{
    const STATUS_ACTIVE = 0;
    const STATUS_PAUSE = 5;
    const STATUS_PART_PAUSE = 10;
    const STATUS_DELETE = 15;
    const STATUS_ARCHIVE = 20;

    public $id;
    public $code;
    public $label;
    public $class;

    public function __construct($id, $code, $label, $class)
    {
        $this->id = $id;
        $this->code = $code;
        $this->label = $label;
        $this->class = $class;
    }

    public function __toString()
    {
        return Html::tag('span', $this->label, [
            'class' => 'label label-' . $this->class
        ]);
    }

    public static function createById($id)
    {
        if (isset(self::map()[$id]) && $status = self::map()[$id]) {
            return new self($id, $status['code'], $status['label'], $status['class']);
        } else {
            throw new NotSupportedException('Статус не найден');
        }
    }

    public static function createByCode($code)
    {
        foreach (self::map() as $id => $status) {
            if ($status['code'] == $code) {
                return new self($id, $code, $status['label'], $status['class']);
            }
        }

        throw new NotSupportedException('Статус не найден');
    }

    public static function hasActive($id)
    {
        switch ($id) {
            case self::STATUS_ACTIVE:
                return true;
            case self::STATUS_PAUSE:
                return true;
            case self::STATUS_PART_PAUSE:
                return true;
            case self::STATUS_DELETE:
                return false;
            case self::STATUS_ARCHIVE:
                return false;
            default:
                return false;
        }
    }

    public static function map()
    {
        return [
            self::STATUS_ACTIVE => [
                'code' => 'green',
                'label' => 'Действует',
                'class' => 'success'
            ],
            self::STATUS_PAUSE => [
                'code' => 'orange',
                'label' => 'Приостановлен',
                'class' => 'warning'
            ],
            self::STATUS_PART_PAUSE => [
                'code' => 'lorange',
                'label' => 'Частично приостановлен',
                'class' => 'info'
            ],
            self::STATUS_DELETE => [
                'code' => 'red',
                'label' => 'Аннулирован',
                'class' => 'danger'
            ],
            self::STATUS_ARCHIVE => [
                'code' => 'purple',
                'label' => 'Архивный',
                'class' => 'danger'
            ]
        ];
    }

    public static function getList()
    {
        $result = [];
        foreach (self::map() as $id => $item) {
            $result[$id] = $item['label'];
        }

        return $result;
    }


}