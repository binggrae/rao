<?php

namespace core\entities\queries;

use core\entities\Certificate;
use core\entities\CertificateStatus;
use yii\db\ActiveQuery;

class CertificateQuery extends ActiveQuery
{

    public function byUid($uid)
    {
        $t = Certificate::tableName();

        $this->andWhere(["{$t}.uid" => $uid]);

        return $this;
    }


    public function byId($id)
    {
        $t = Certificate::tableName();

        $this->andWhere(["{$t}.id" => $id]);

        return $this;
    }

    public function byJob($job)
    {
        $t = Certificate::tableName();

        $this->andWhere(["{$t}.job" => $job]);

        return $this;
    }

    public function notDelete()
    {
        $t = Certificate::tableName();

        $this->andWhere(["!=", "{$t}.status", [CertificateStatus::STATUS_DELETE, CertificateStatus::STATUS_ARCHIVE]]);

        return $this;
    }


    public function sortId($desc = true)
    {
        $t = Certificate::tableName();

        $this->orderBy(["{$t}.id" => $desc ? SORT_DESC : SORT_ASC]);

        return $this;
    }


}

