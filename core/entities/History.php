<?php

namespace core\entities;

use core\helpers\HistoryHelper;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "histories".
 *
 * @property int $id
 * @property int $cert_id
 * @property string $type
 * @property int $created_at
 * @property string $data
 * @property string $name
 * @property string $value
 *
 *
 * @property Certificate $certificate
 */
class History extends \yii\db\ActiveRecord
{

    const TYPE = null;


    public static function create($cert_id, $value)
    {
        $history = new static();
        $history->cert_id = $cert_id;
        $history->type = static::TYPE;
        $history->created_at = time();
        $history->data = $value;

        return $history;
    }

    public function getName()
    {
        return HistoryHelper::getLabel($this->type);
    }

    public function getValue()
    {
        return $this->data;
    }

    public function init()
    {
        $this->type = static::TYPE;
        parent::init();
    }

    public static function find()
    {
        return new HistoryQuery(get_called_class(), ['type' => static::TYPE]);
    }

    public static function instantiate($row)
    {
        switch ($row['type']) {
            case StatusHistory::TYPE:
                return new StatusHistory();
            case AddFileHistory::TYPE:
                return new AddFileHistory();
            case UpdateFileHistory::TYPE:
                return new UpdateFileHistory();
            default:
                return new self;
        }
    }

    public static function tableName()
    {
        return 'histories';
    }


    public function rules()
    {
        return [
            [['cert_id', 'type', 'created_at'], 'required'],
            [['cert_id', 'created_at'], 'integer'],
            [['data'], 'safe'],
            [['type'], 'string', 'max' => 255],
            [['cert_id'], 'exist', 'skipOnError' => true, 'targetClass' => Certificate::className(), 'targetAttribute' => ['cert_id' => 'id']],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cert_id' => 'Лаборатория',
            'type' => 'Тип события',
            'created_at' => 'Дата',
            'data' => 'Значение',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificate()
    {
        return $this->hasOne(Certificate::className(), ['id' => 'cert_id']);
    }

    public function beforeSave($insert)
    {
        $this->type = static::TYPE;
        return parent::beforeSave($insert);
    }

}


class HistoryQuery extends ActiveQuery
{
    public $type;

    public function prepare($builder)
    {
        if ($this->type !== null) {
            $this->andWhere(['type' => $this->type]);
        }
        return parent::prepare($builder);
    }

}
