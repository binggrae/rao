<?php

namespace core\entities;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "regions".
 *
 * @property int $id
 * @property string $alias
 * @property string $title
 *
 * @property Certificate[] $certificates
 */
class Region extends \yii\db\ActiveRecord
{

    public static function create($alias, $title = '')
    {
        $region = new self();
        $region->alias = $alias;
        $region->title = $title ?: $alias;

        return $region;
    }

    public static function tableName()
    {
        return 'regions';
    }

    
    public function rules()
    {
        return [
            [['alias', 'title'], 'required'],
            [['alias', 'title'], 'string', 'max' => 255],
        ];
    }

    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCertificates()
    {
        return $this->hasMany(Certificate::className(), ['region_id' => 'id']);
    }

    public static function getList()
    {
        return ArrayHelper::merge([null => '-- Любой --' ], ArrayHelper::map(self::find()->orderBy('title')->all(), 'id', 'title'));
    }
}
