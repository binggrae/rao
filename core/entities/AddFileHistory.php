<?php


namespace core\entities;


class AddFileHistory extends History
{
    const TYPE = 'add_file';


    public function getValue()
    {
        $file = File::find()->where(['id' => $this->data])->one();
        if (!$file) {
            return null;
        }

        return $file->type . '/' . $file->name;
    }


}