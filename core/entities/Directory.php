<?php

namespace core\entities;


use League\Flysystem\Config;

/**
 * This is the model class for table "directory".
 *
 * @property int $id
 * @property string $uid
 * @property string $path
 */
class Directory extends \yii\db\ActiveRecord
{

    public static function getUidByPath($path)
    {
        $config = new Config();
        $dirs = explode('/', $path);
        $root = '';

        $path = '/';
        foreach ($dirs as $dir) {
            if ($dir == '') {
                continue;
            }

            $path .= $dir . '/';

            $directory = Directory::find()->where(['path' => $path])->one();
            if (!$directory) {
                $folder = \Yii::$app->gdrive->getAdapter()->createDir($root . '/' . $dir, $config);
                $directory = Directory::create($path, $folder['path']);
                $directory->save();
            }

            $root = $directory->uid;
        }

        return $root;
    }

    public static function create($path, $uid)
    {
        $directory = new self();
        $directory->path = $path;
        $directory->uid = array_pop(explode('/', $uid));

        return $directory;
    }

    public static function tableName()
    {
        return 'directory';
    }


    public function rules()
    {
        return [
            [['uid', 'path'], 'required'],
            [['uid', 'path'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Uid',
            'path' => 'Path',
        ];
    }
}
