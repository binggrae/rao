<?php


namespace core\entities;


class StatusHistory extends History
{
    const TYPE = 'status';



    public function getValue()
    {
        return $this->certificate->status->label;
    }




}