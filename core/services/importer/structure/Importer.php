<?php


namespace core\services\importer\structure;


use core\repositories\AreaRepository;
use core\repositories\RegionRepository;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

class Importer
{

    private $file;


    public function __construct($file)
    {
        $this->file = $file;
    }


    public function getRows()
    {
        $header = null;
        if (($handle = fopen($this->file, "r")) !== FALSE) {
            while (($str = fgets($handle)) !== FALSE) {
                $math = preg_replace_callback('/\[([^\[]*[^\[\]])\]/', function($math) {
                    return str_replace("\"\"", "'", $math[1]);
                }, $str);
                $math = str_replace('\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\""', '`', $math);
                $math = str_replace('\\""', '`', $math);

                $data = str_getcsv($math);
                if (!$header) {
                    foreach ($data as $title) {
                        $title = str_replace('А', 'A', $title);
                        $title = str_replace(' ', '_', $title);
                        $title = strtolower($title);
                        $title = explode('/', $title)[0];
                        $header[] = $title;
                    }
                    continue;
                }
                $num = count($data);
                $result = [];
                for ($c = 0; $c < $num; $c++) {
                    if($header[$c]) {
                        $result[$header[$c]] = $data[$c];
                    }
                }
                yield $result;
            }
            fclose($handle);
        }
    }

    public function clear()
    {
        if (is_file($this->file)) {
            unlink($this->file);
        }
    }


}