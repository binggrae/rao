<?php


namespace core\services\importer\structure;

class Row
{

    public $id;
    public $areas;
    public $region;

    public function __construct($id, $areas, $region)
    {

        $this->id = $id;
        $this->areas = $areas;
        $this->region = $region;
    }
}