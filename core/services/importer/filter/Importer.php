<?php


namespace core\services\importer\filter;


use core\repositories\AreaRepository;
use core\repositories\RegionRepository;
use yii\web\UploadedFile;

class Importer
{
    /** @var \PHPExcel_Worksheet */
    private $sheet;

    /** @var RegionRepository */
    private $regions;
    private $regions_data = [];

    /** @var AreaRepository */
    private $areas;
    private $areas_data = [];

    private $file;


    public function __construct($file)
    {
        $this->regions = \Yii::$container->get(RegionRepository::class);
        $this->areas = \Yii::$container->get(AreaRepository::class);

        $reader = new \PHPExcel_Reader_Excel2007();
        $reader->setReadDataOnly(true);

        $excel = $reader->load($file);
        $this->sheet = $excel->getActiveSheet();
        $this->file = $file;
    }

    /**
     * @return \Generator|Row[]
     */
    public function getRows()
    {
        $highestRow = $this->sheet->getHighestRow();
        for ($row = 2; $row < $highestRow; ++$row) {
            yield new Row(
                $this->sheet->getCellByColumnAndRow(0, $row)->getValue(),
                $this->getAreas($this->sheet->getCellByColumnAndRow(1, $row)->getValue()),
                $this->getRegion($this->sheet->getCellByColumnAndRow(2, $row)->getValue())
            );
        }
    }

    public function clear()
    {
        if(is_file($this->file)) {
            unlink($this->file);
        }
    }

    private function getRegion($value)
    {
        if (!isset($this->regions_data[$value])) {
            $this->regions_data[$value] = $this->regions->getByAlias($value);
        }
        return $this->regions_data[$value];
    }

    private function getRegions($value)
    {
        $result = [];
        foreach (explode(',', $value)as $item) {
            if (!$item) {
                continue;
            }
            $name = trim($item);
            if (!isset($result[$name])) {
                $result[$name] = $this->getRegion($name);
            }
        }

        return array_values($result);
    }

    private function getAreas($value)
    {
        $result = [];
        foreach (preg_split('/(?=[А-Я])/u', $value) as $item) {
            if (!$item) {
                continue;
            }
            if (!isset($result[$item])) {
                $result[$item] = $this->getArea($item);
            }
        }

        return array_values($result);
    }

    private function getArea($value)
    {
        if (!isset($this->areas_data[$value])) {
            $this->areas_data[$value] = $this->areas->getByAlias($value);
        }
        return $this->areas_data[$value];
    }


}