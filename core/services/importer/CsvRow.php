<?php


namespace core\services\importer;


/**
 * Class CsvRow
 * @package core\services\importer
 * @property $uid
 * @property $date
 */
class CsvRow
{
    private $headers;
    private $row;

    public function __construct($headers, $row)
    {
        $this->headers = explode(';', $headers);
        $this->row = explode(';', $row);

    }

    public function __get($name)
    {
        if ($alias = $this->map($name)) {
            foreach ($this->headers as $idx => $header) {
                if($alias == $header) {
                    return $this->row[$idx];

                }
            }
        }
        return null;
    }


    public function map($name = null)
    {
        //Task;Answer;Reg_number;On_reg_dat;Last_Update_dat;Docs;Links;Status;Comment
        $map = [
            'uid' => 'Reg_number',
            'date' => 'On_reg_dat'
        ];
        return $name ?
            isset($map[$name]) ?
                $map[$name]
                : null
            : $map;
    }


}