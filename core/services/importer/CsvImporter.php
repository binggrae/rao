<?php


namespace core\services\importer;


use core\entities\Certificate;

class CsvImporter
{
    private $file;

    public function __construct($path)
    {
        $this->file = fopen($path, 'r');
    }


    /**
     * @throws \Exception
     */
    public function import()
    {
        try {
            foreach ($this->read() as $row) {
                if ($row->uid) {
                    $certificate = Certificate::create($row->uid, $row->date);
                }
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @return \Generator|void
     */
    private function read()
    {
        $headers = array_map('trim', fgetcsv($this->file, 4096));
        while (!feof($this->file)) {
            $row = array_map('trim', (array)fgetcsv($this->file, 4096));
//            $row[0] = mb_convert_encoding($row[0], 'UTF-8', 'cp1251');
            if (count($headers) !== count($row)) {
                continue;
            }
            $row = new CsvRow($headers[0], $row[0]);
            yield $row;
        }
        return;

    }

}