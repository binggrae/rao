<?php


namespace core\services\parser\pages;


class InitPage
{

    private $pq;

    public function __construct($html)
    {
        $this->pq = \phpQuery::newDocumentHTML(mb_convert_encoding($html, 'UTF-8', 'cp1251'));
    }

    public function getId()
    {
        return str_replace('id_', '', $this->pq->find('#bodyTableData tr.b-tr-body')->attr('id'));
    }


    public function getStatus()
    {
        return str_replace('status-ico ', '', $this->pq->find('#bodyTableData .status-ico')->attr('class'));
    }

}