<?php


namespace core\services\parser\pages;


use core\services\parser\elements\ScopeFile;
use yii\helpers\Json;

class ViewPage
{

    private $pq;

    public function __construct($html)
    {
        $this->pq = \phpQuery::newDocumentHTML(mb_convert_encoding($html, 'UTF-8', 'cp1251'));
    }


    public function getFiles()
    {
        $files = [];
        foreach ($this->pq->find('.type-file_form') as $tr) {
            $pqTr = pq($tr);
            $pqFile =  $pqTr->find('.file_name');

            $file = new ScopeFile(
                $pqTr->find('.form-left-col')->text(),
                $pqFile->text(),
                $pqFile->attr('data-id')
            );

            if($file->uid) {
                $files[] = $file;
            }
        }
        return $files;
    }


    public function getStatus()
    {
        return 1;
    }

    public function getTypeAl()
    {
        return trim($this->pq->find('#cgroup-type_accred_person-cert_org')->text())
            . trim($this->pq->find('#cgroup-type_accred_person-test_lab')->text())
            . trim($this->pq->find('#cgroup-type_accred_person-inspect_org')->text())
            ;
    }

    public function getTypeAp()
    {

        return mb_strtolower(trim($this->pq->find('#cgroup-type_applicant-ul')->text()));
    }

    public function getFullNameAp()
    {
        return trim($this->pq->find('#a_applicant-ral-applicant_j_data-full_name .form-right-col')->text());
    }

    public function getFioRukAp()
    {
        return trim($this->pq->find('#a_applicant-ral-applicant_j_data-director_name .form-right-col')->text());
    }

    public function getDolRukAp()
    {
        // sdfsdf
        return trim($this->pq->find('#a_applicant-ral-applicant_j_data-director_name .form-right-col')->text());
    }

    public function getAddressAp()
    {
        return trim($this->pq->find('#a_applicant-ral-applicant_j_data-address .form-right-col')->text());
    }

    public function getPhoneAp()
    {
        $pq = $this->pq->find('#a_applicant-ral-applicant_j_data-phone .form-right-col');
        if($pq->find('.array-field')) {
            $phones = [];
            foreach ($pq->find('.array-field') as $item) {
                $phones[] = trim(pq($item)->text());
            }
            return implode(',', $phones);
        } else {
            return trim($pq->text());
        }


//        return trim($this->pq->find('#a_applicant-ral-applicant_j_data-phone .form-right-col')->text());
    }

    public function getEmailAp()
    {
        return trim($this->pq->find('#a_applicant-ral-applicant_j_data-email .form-right-col')->text());
    }

    public function getOgrn()
    {
        return trim($this->pq->find('#a_applicant-ral-applicant_j_data-ogrn .form-right-col')->text());
    }

    public function getInnAp()
    {
        return trim($this->pq->find('#a_applicant-ral-applicant_j_data-inn .form-right-col')->text());
    }

    public function getFullNameAl()
    {
        return trim($this->pq->find('#a_accred_person [fsa-id="full_name"] .form-right-col')->text());
    }

    public function getFioRukAl()
    {
        return trim($this->pq->find('#a_accred_person [fsa-id="director_name"] .form-right-col')->text());
    }

    public function getAddressAl()
    {
        return trim($this->pq->find('#a_accred_person [fsa-id="address"] .form-right-col')->text());
    }


    public function getOnRegDat()
    {
        return trim($this->pq->find('#a_start_date .form-right-col')->text());
    }

    public function getAkkredOrderNum()
    {
        return trim($this->pq->find('#a_init_accred-ral-init_accred-order_number .form-right-col')->text());
    }

    public function getAkkredOrderDate()
    {
        return trim($this->pq->find('#a_init_accred-ral-init_accred-order_date .form-right-col')->text());
    }

    public function getOblAkkAl()
    {
        $result = [];
        $result['Основная'] = [
            'Область аккредитации' => $this->pq->find('#a_accred_scope [fsa-id="accred_scope_text"] .form-right-col')->text(),
            'Технический регламент ТС' => $this->pq->find('#a_accred_scope [fsa-id="tech_reg_ts"] .form-right-col')->text(),
            'Коды ОКП' => $this->pq->find('#a_accred_scope [fsa-id="okp_text"] .form-right-col')->text(),
            'Коды ОКУН' => $this->pq->find('#a_accred_scope [fsa-id="okun_text"] .form-right-col')->text(),
        ];
        foreach ($this->pq->find('#a_cert_changes [fsa-id="accred_scope"]') as $item) {
            $pq = pq($item);
            if(strpos($pq->attr('id'), 'expansion')) {
                if(!isset($result['Расширенная'])) {
                    $result['Расширенная'] = [];
                }

                $result['Расширенная'][] = [
                    'Область аккредитации' => $pq->find('[fsa-id="accred_scope_text"] .form-right-col')->text(),
                    'Технический регламент ТС' => $pq->find('[fsa-id="tech_reg_ts"] .form-right-col')->text(),
                    'Коды ОКП' => $pq->find('[fsa-id="okp_text"] .form-right-col')->text(),
                    'Коды ОКУН' => $pq->find('[fsa-id="okun_text"] .form-right-col')->text(),

                ];
            }
        }
        return Json::encode($result);
    }

}