<?php


namespace core\services\parser\pages;


class SearchPage
{

    private $pq;

    private $is_next = false;

    public function __construct($html)
    {
        $html = mb_convert_encoding($html, 'UTF-8', 'cp1251');

//        file_put_contents(\Yii::getAlias('@console/runtime/'. uniqid() .'.html'), $html);

        $this->pq = \phpQuery::newDocumentHTML($html);
        $this->is_next = !!$this->pq->find('.cl_last')->attr('onclick');
    }


    public function hasNext()
    {
        return $this->is_next;
    }

    public function getList()
    {
        foreach ($this->pq->find('#bodyTableData tr.b-tr-body') as $tr) {
            $trPq = pq($tr);
            $id = str_replace('id_', '',$trPq->attr('id'));
            yield [
                'id' => $id,
                'uid' => trim($trPq->find('#tbl_cert_num_' . $id)->text()),
                'status' => str_replace('status-ico ', '', $trPq->find('#tbl_status_'. $id .' .status-ico')->attr('class')),
            ];
        }
    }

    public function close()
    {
        $this->pq = null;
        \phpQuery::unloadDocuments();
    }

}