<?php


namespace core\services\parser\elements;


class ScopeFile
{

    public $type;
    public $name;
    public $uid;

    public function __construct($alias, $name, $uid)
    {
        $this->type = $this->getType(trim($alias));
        $this->name = trim($name);
        $this->uid = $uid;
    }

    private function getType($alias)
    {
        $aliases = [
            'Скан-копия области аккредитации' => 'confirm',
            'Скан-копия расширяемой области аккредитации' => 'expansion',
            'Скан-копия сокращаемой области аккредитации' => 'reduction',
        ];
        return $aliases[$alias];
    }


}