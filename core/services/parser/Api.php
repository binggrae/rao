<?php


namespace core\services\parser;


use core\entities\Certificate;
use core\entities\File;
use core\services\parser\actions\FileAction;
use core\services\parser\actions\InitAction;
use core\services\parser\actions\LoadAction;
use core\services\parser\actions\SearchAction;

class Api
{

    /** @var InitAction */
    private $init;

    /** @var LoadAction */
    private $load;

    /** @var FileAction */
    private $file;

    /** @var SearchAction */
    private $search;

    public function __construct(
        InitAction $init,
        LoadAction $load,
        FileAction $file,
        SearchAction $search)
    {
        $this->init = $init;
        $this->load = $load;
        $this->file = $file;
        $this->search = $search;
    }

    /**
     * @param Certificate[] $certificates
     * @return bool
     * @throws \yii\base\NotSupportedException
     */
    public function init($certificates)
    {
        return $this->init->run($certificates);
    }

    /**
     * @param Certificate[] $certificates
     * @return bool
     * @throws \yii\base\NotSupportedException
     * @throws \yii\db\Exception
     */
    public function load($certificates)
    {
        return $this->load->run($certificates);
    }

    /**
     * @param File $file
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     */
    public function getFile($file)
    {
        return $this->file->run($file);
    }

    public function search()
    {
        return $this->search->run();
    }


}