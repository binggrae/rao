<?php


namespace core\services\parser\jobs;


use core\entities\File;
use core\helpers\FileHelper;
use core\services\parser\Api;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class FileJob extends BaseObject implements JobInterface
{

    public $id;

    /** @var Api */
    private $api;

    public function execute($queue)
    {
        $this->api = \Yii::$container->get(Api::class);

        $file = File::find()->where(['id' => $this->id])->one();
        if(!$file) {
            return;
        }
        $file->status = FileHelper::STATUS_PROGRESS;
        $file->save();

        $this->api->getFile($file);
    }


}