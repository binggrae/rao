<?php


namespace core\services\parser\jobs;


use core\entities\Certificate;
use core\helpers\CertificateHelper;
use core\services\parser\Api;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class InitJob extends BaseObject implements JobInterface
{

    public $ids;

    /** @var Api */
    private $api;


    public function execute($queue)
    {
        $this->api = \Yii::$container->get(Api::class);

        $certificates = Certificate::find()->byId($this->ids)->all();

        foreach ($certificates as $certificate) {
            $certificate->job = CertificateHelper::STATUS_PROGRESS;
            $certificate->save();
        }

        $this->api->init($certificates);
    }


}