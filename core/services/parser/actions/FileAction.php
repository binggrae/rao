<?php


namespace core\services\parser\actions;

use core\entities\File;
use core\helpers\FileHelper;
use core\services\Client;
use League\Flysystem\Config;
use yii\helpers\BaseFileHelper;

class FileAction
{

    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param File $file
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     */
    public function run(&$file)
    {
        try {
            $response = $this->client->get($file->url)->send();
            if ($response->isOk) {
                file_put_contents($file->rootPath, $response->content);
            }

            $file->extension = $this->getExtension($file->rootPath);
            if ($file->extension) {
                $file->size = filesize($file->rootPath);
                $file->gpath = $file->gpath ? $this->update($file) : $this->write($file);

                $file->status = FileHelper::STATUS_COMPLETE;
                $file->save();
            } else {
                $file->delete();
            }

            $file->clearTmp();
            return true;
        } catch (\Exception $e) {
            $file->status = FileHelper::STATUS_ERROR;
            $file->save();
            throw $e;
        }
    }


    /**
     * @param File $file
     * @return string
     */
    private function write($file)
    {
        return $this->upload($file, $this->getDrivePath($file));

    }

    /**
     * @param File $file
     * @return string
     */
    private function update($file)
    {
        return $this->upload($file, $file->gpath);
    }

    /**
     * @param File $file
     * @param $path
     * @return string
     */
    private function upload($file, $path)
    {
        $config = new Config();
        $stream = fopen($file->rootPath, 'r+');

        $uploaded = \Yii::$app->gdrive->getAdapter()->writeStream($path, $stream, $config);

        return $uploaded['path'];
    }

    /**
     * @param File $file
     * @return string
     */
    private function getDrivePath($file)
    {
        $dir = $file->certificate->uid . '_' .
            FileHelper::getAlias($file->type) . '_' .
            $file->uid . '.' .
            $file->extension;

        return $dir;
    }

    private function getExtension($path)
    {
        $finfo = finfo_open(FILEINFO_MIME_TYPE); // возвращает mime-тип
        $mime = finfo_file($finfo, $path);
        finfo_close($finfo);

        $ext = BaseFileHelper::getExtensionsByMimeType($mime);

        if (!isset($ext[0]) || $ext[0] != 'pdf') {
            return null;
        }

        return 'pdf';
    }

}