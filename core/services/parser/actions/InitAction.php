<?php


namespace core\services\parser\actions;


use core\entities\Certificate;
use core\entities\CertificateStatus;
use core\helpers\CertificateHelper;
use core\services\Client;
use core\services\parser\forms\ViewForm;
use core\services\parser\pages\InitPage;

class InitAction
{

    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {

        $this->client = $client;
    }

    /**
     * @param Certificate[] $certificates
     * @return bool
     * @throws \yii\base\NotSupportedException
     */
    public function run($certificates)
    {

        $requests = [];
        foreach ($certificates as $certificate) {
            $form = new ViewForm($certificate->uid);
            $requests[$certificate->id] = $this->client->post($form->getLink(), $form->getData());
        }

        $responses = $this->client->batch($requests);

        foreach ($certificates as $certificate) {
            if ($responses[$certificate->id]->isOk) {
				try {
                $page = new InitPage($responses[$certificate->id]->content);

                $certificate->changeLink($page->getId());
                $certificate->changeStatus(CertificateStatus::createByCode($page->getStatus()));

                $certificate->job = CertificateHelper::STATUS_LOADED;
				$certificate->save();
				} catch(\Exception $e) {
					$certificate->delete();
					continue;
				}
            } else {
                $certificate->job = CertificateHelper::STATUS_ERROR;
				$certificate->save();
            }
            
        }

        return true;

    }

}