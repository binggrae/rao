<?php


namespace core\services\parser\actions;


use core\entities\Certificate;
use core\entities\CertificateStatus;
use core\helpers\CertificateHelper;
use core\services\Client;
use core\services\parser\forms\SearchForm;
use core\services\parser\forms\ViewForm;
use core\services\parser\pages\InitPage;
use core\services\parser\pages\SearchPage;

class SearchAction
{

    /** @var Client */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }


    public function run()
    {
        $form = new SearchForm();
        do {
            $request = $this->client->post(SearchForm::URL, $form->getData())->send();
            $page = new SearchPage($request->content);
            var_dump('Page: ' . $form->page_noid_);

            $ids = [];
            foreach ($page->getList() as $item) {
                $certificate = Certificate::find()->where(['link' => $item['id']])->one();
                if (!$certificate) {
                    $certificate = Certificate::create($item['uid'], date('d.m.Y H:i:s'));
                }
                $certificate->changeLink($item['id']);
                $certificate->changeStatus(CertificateStatus::createByCode($item['status']));
                $certificate->job = CertificateHelper::STATUS_COMPLETE;
                $certificate->save();

                if(CertificateStatus::hasActive($certificate->status->id)) {
                    $ids[] = $certificate->id;
                }
            }

            $page->close();

            yield $ids;

            $form->incPage();
        } while ($page->hasNext());
    }

}