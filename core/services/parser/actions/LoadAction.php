<?php


namespace core\services\parser\actions;


use core\entities\Certificate;
use core\entities\CertificateStatus;
use core\entities\Structure;
use core\helpers\CertificateHelper;
use core\services\Client;
use core\services\parser\forms\ViewForm;
use core\services\parser\jobs\FileJob;
use core\services\parser\pages\InitPage;
use core\services\parser\pages\ViewPage;
use yii\helpers\VarDumper;
use yii\httpclient\Response;

class LoadAction
{

    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param Certificate[] $certificates
     * @return bool
     * @throws \yii\db\Exception
     */
    public function run($certificates)
    {
        $requests = [];
        foreach ($certificates as $certificate) {
            $form = new ViewForm($certificate->uid);
            if ($certificate->job == CertificateHelper::STATUS_LOADED) {
                $requests['init'][$certificate->id] = $this->client->post($form->getLink(), $form->getData());
            }

            $requests['view'][$certificate->id] = $this->client->get($certificate->url);
        }

        if ($requests['init']) {
            $responses['init'] = $this->client->batch($requests['init']);
        }
        $responses['view'] = $this->client->batch($requests['view']);

        foreach ($certificates as $certificate) {
            $transaction = \Yii::$app->db->beginTransaction();

            try {
                if ($responses['init']) {
                    $this->parseInitRequest($responses['init'][$certificate->id], $certificate);
                }
                $this->parseViewRequest($responses['view'][$certificate->id], $certificate);
                foreach ($certificate->files_ids as $id) {
                    \Yii::$app->queue->push(new FileJob([
                        'id' => $id
                    ]));
                }

                $certificate->job = CertificateHelper::STATUS_COMPLETE;
                $certificate->save();

                $transaction->commit();
            } catch (\DomainException $e) {
                var_dump($e->getMessage());
                $transaction->rollBack();
                $certificate->job = CertificateHelper::STATUS_ERROR;
                $certificate->save();
            }
        }

        return true;

    }

    /**
     * @param Response $response
     * @param Certificate $certificate
     * @return bool
     * @throws \yii\base\NotSupportedException
     * @throws \yii\httpclient\Exception
     */
    private function parseInitRequest($response, $certificate)
    {
        if ($response->isOk) {
            $page = new InitPage($response->content);
            $certificate->changeLink($page->getId());
            $certificate->changeStatus(CertificateStatus::createByCode($page->getStatus()));

            return true;
        } else {
            throw new \DomainException('request error ' . $response->getStatusCode());
        }
    }


    /**
     * @param Response $response
     * @param Certificate $certificate
     * @return bool
     */
    private function parseViewRequest($response, $certificate)
    {

        if ($response->isOk) {
            $page = new ViewPage($response->content);
            $certificate->addFiles($page->getFiles());
            $structure = $certificate->structure ?: new Structure();
//            $structure = new Structure();

//            var_dump($structure->attributes);

            $structure->on_reg_dat = $page->getOnRegDat();
            $structure->akkred_order_num = $page->getAkkredOrderNum();
            $structure->akkred_order_dat = $page->getAkkredOrderDate();
            $structure->reg_number = $certificate->uid;
            $structure->status = $certificate->status->label;
            $structure->type_al = $page->getTypeAl();
            $structure->type_ap = $page->getTypeAp();
            $structure->full_name_ap = $page->getFullNameAp();
            $structure->fio_ruk_ap = $page->getFioRukAp();
            $structure->address_ap = $page->getAddressAp();
            $structure->phone_ap = $page->getPhoneAp();
            $structure->email_ap = $page->getEmailAp();
            $structure->ogrn = $page->getOgrn();
            $structure->inn_ap = $page->getInnAp();
            $structure->full_name_al = $page->getFullNameAl();
            $structure->fio_ruk_al = $page->getFioRukAl();
            $structure->address_al = $page->getAddressAl();
            $structure->address_delo_ap = $structure->address_al;
            $structure->obl_akk_al = $page->getOblAkkAl();

            $structure->certificate_id = $certificate->id;

            if(!$structure->save()) {
                var_dump($structure->getErrors());
            };


            return true;
        } else {
            throw new \DomainException('request error ' . $response->getStatusCode());
        }

    }

}