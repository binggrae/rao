<?php


namespace core\services\parser\forms;


class SearchForm
{
    const URL = 'http://public.fsa.gov.ru/table_rao_pub_rf/index.php';

    public $ajax = 'main';
    public $idid_ = 'content-table';
    public $sortid_ = 'DESC';
    public $orderid_ = 3;
    public $Divid_ = 'tableContent-content-table';
    public $Formid_ = 'formContentcontent-table';
    public $ajaxid_ = "";
    public $rowActid_ = "";
    public $page_byid_ = 50;
    public $page_noid_ = 0;
    public $prefixid_ = 'id_';
    public $findid_ = '';
    public $pageGoid_ = 2;
    public $html_id = 'tableContent-content-table';
    public $ajaxId = '8109010631';
    public $loadedExtraFiles = '35782l35784l35786l35788l35790l35798l35800l35802l35804l35806l35808l35810l35812l1124l35836l35818l35836l35818l35820l35822l35824l35826l35828l35830l35832l35834l35842l35844l35848l35852l35836l35818l35866l1166l1168l1170l1172l1174l1176l1178l1184l1186l1188l1190l1192l1194l1196l1198l1200l35784l35858l1204l1206l1188l1208l35854l35856l1216l1218l1220l1222l1222l1224l1078l1080l1082l1084l1086l1088l1180l1182l1212l1214l1210';


    public function getData()
    {
        return get_object_vars($this);
    }

    public function incPage()
    {
        $this->page_noid_++;
    }
}