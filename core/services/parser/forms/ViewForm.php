<?php


namespace core\services\parser\forms;


class ViewForm
{
    private $uid;

    public function __construct($uid)
    {
        $this->uid = $uid;
    }


    public function getData()
    {
        return [
            'ajax' => 'main',
            'action' => 'search',
            'input_1' => $this->uid,
        ];
    }

    public function getLink()
    {
        return 'http://public.fsa.gov.ru/table_rao_pub_rf/index.php';
    }
}