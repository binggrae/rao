<?php


namespace core\services\csv;


interface ExporterInterface
{
    public function print($titles);

}