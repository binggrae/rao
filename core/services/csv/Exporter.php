<?php


namespace core\services\csv;


class Exporter
{

    private $handleUtf;
    private $titles;

    public function __construct($name, $titles)
    {
        $this->handleUtf = fopen(\Yii::getAlias("@common/data/result.csv"), 'w');
        $this->titles = $titles;

        $this->write($titles);
    }

    public function addProduct(ExporterInterface $product)
    {
        $this->write($product->print($this->titles));
    }

    public function close()
    {
        fclose($this->handleUtf);

    }

    private function write($data)
    {
        $win = [];
        foreach ($data as $key => $value) {
            $win[$key] = $this->toWindow($value);
        }
        fputcsv($this->handleUtf, $win, ';');
    }

    private function toWindow($str)
    {
        try {
            $str = str_replace(["\n\r", "\r\n", "\n", "\r"], ' ', $str);
            $str = substr($str, 0, 32724);
            $str = htmlspecialchars($str);
            return mb_convert_encoding($str, "windows-1251");
        } catch (\Exception $e) {
            return 'convert encoding error';
        }
    }
}