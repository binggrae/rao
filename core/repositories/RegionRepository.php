<?php


namespace core\repositories;


use core\entities\Region;

class RegionRepository
{

    public function getByAlias($alias)
    {
        $region = Region::find()->where(['alias' => $alias])->one();
        if(!$region) {
            $region = Region::create($alias);
        }


        return $region;
    }


}