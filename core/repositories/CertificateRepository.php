<?php


namespace core\repositories;


use core\entities\Certificate;

class CertificateRepository
{

    public function getByUid($uid)
    {
        return Certificate::find()->byUid($uid)->one();
    }


    public function save(Certificate $certificate)
    {
        try {
            $certificate->save();
        } catch (\Exception $e) {
            throw new \DomainException('Ошибка сохранения');
        }
    }

}