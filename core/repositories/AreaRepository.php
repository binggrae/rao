<?php


namespace core\repositories;


use core\entities\Area;

class AreaRepository
{

    public function getByAlias($alias)
    {
        $region = Area::find()->where(['alias' => $alias])->one();
        if(!$region) {
            $region = Area::create($alias);
        }


        return $region;
    }


}