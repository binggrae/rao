<?php


namespace core\forms\certificate;


use core\entities\Certificate;
use yii\base\Model;

class CertificateEditForm extends Model
{
    public $region_id;

    public $areas;

    /** @var Certificate */
    public $certificate;

    public function __construct(Certificate $certificate, array $config = [])
    {
        $this->certificate = $certificate;

        parent::__construct($config);
    }

    public function rules()
    {
        return [

        ];
    }

}