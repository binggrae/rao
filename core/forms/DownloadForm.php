<?php


namespace core\forms;


use core\entities\Structure;
use yii\base\Model;
use yii\helpers\Json;
use yii\web\Cookie;

/**
 *
 * @property array $listTitle
 * @property array $types
 * @property array $listAttributes
 */
class DownloadForm extends Model
{
    public $list = [];

    public function load($data, $formName = null)
    {
        $return = parent::load($data, $formName);
        if ($this->list == '') {
            $this->list = [];
        }

        if(empty($this->list)) {
            foreach (Json::decode(\Yii::$app->request->cookies->get('filters')) as $item) {
                $this->list[] = $item;
            };
        }


        return $return;
    }

    public function rules()
    {
        return [
            [['list'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'list' => 'Поля для экспорта'
        ];
    }

    public function save()
    {
        $result = [];
        foreach ($this->list as $item) {
            $result[] = $item;
        }

        \Yii::$app->response->cookies->add(new Cookie([
            'name' => 'filters',
            'value' => Json::encode($result),
            'expire' => time() + (60 * 60 * 24 * 365)
        ]));
    }


    public function getListTitle()
    {
        $titles = [
            'reg_number',
            'on_reg_dat',
            'last_update_dat',
            'url',
            'status',
            'region_name',
            'areas_name'
        ];

        foreach ($this->list as $attribute) {
            $titles[] = $attribute;
        }
        return $titles;
    }

    public function getTypes()
    {
        $types = [];
        for ($i = 0; $i < count($this->listTitle); $i++) {
            $types[] = 's';
        }

        return $types;
    }


    public function getListAttributes()
    {
        $attr = [
            'uid',
            'xlsCreated',
            'xlsUpdated',
            'url',
            'xlsStatus',
            'xlsRegion',
            'xlsAreas'
        ];

        foreach ($this->list as $attribute) {
            $attr[] = 'structure.' . $attribute;
        }

        return $attr;
    }


}