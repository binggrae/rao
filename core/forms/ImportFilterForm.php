<?php

namespace core\forms;


use yii\base\Model;
use yii\helpers\BaseFileHelper;
use yii\web\UploadedFile;

class ImportFilterForm extends Model
{

    /** @var UploadedFile */
    public $file;
    public $filePath;

    public function rules()
    {
        return [
            [['file'], 'file', 'extensions' => 'xlsx']
        ];
    }

    public function attributeLabels()
    {
        return [
            'file' => 'Файл для импорта'
        ];
    }

    public function save()
    {

        try {
            $dir = \Yii::getAlias('@common/runtime/import/filter/');
            BaseFileHelper::createDirectory($dir);
            $path = $dir . uniqid() . '.' . $this->file->getExtension();
            $this->file->saveAs($path);
            $this->filePath = $path;
            return true;
        } catch (\Exception $e) {
            throw  $e;
        }
    }

}