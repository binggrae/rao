<?php


namespace core\helpers;


use yii\bootstrap\Html;

class FileHelper
{

    public const STATUS_NEW = 0;
    public const STATUS_WAIT = 5;
    public const STATUS_PROGRESS = 10;
    public const STATUS_ERROR = 15;
    public const STATUS_COMPLETE = 20;
    public const STATUS_LOADED = 25;

    public static function getStatusLabel($status = null)
    {
        $labels = [
            self::STATUS_NEW => 'Новый',
            self::STATUS_WAIT => 'Ожидает запуска',
            self::STATUS_PROGRESS => 'В работе',
            self::STATUS_ERROR => 'Ошибка',
            self::STATUS_COMPLETE => 'Завершено',
            self::STATUS_LOADED => 'Загружен',
        ];

        return $status === null ? $labels : $labels[$status];
    }


    public static function getStatusClass($status): string
    {
        $classes = [
            self::STATUS_NEW => 'label-default',
            self::STATUS_LOADED => 'label-default',
            self::STATUS_WAIT => 'label-warning',
            self::STATUS_PROGRESS => 'label-primary',
            self::STATUS_ERROR => 'label-danger',
            self::STATUS_COMPLETE => 'label-success',
        ];

        return $classes[$status];
    }

    public static function getStatusView($status): string
    {
        return Html::tag('span', self::getStatusLabel($status), [
            'class' => 'label ' . self::getStatusClass($status)
        ]);
    }

    public static function isDisabled($status): bool
    {
        return \in_array($status, [
            self::STATUS_WAIT,
            self::STATUS_PROGRESS,
        ], true);
    }

    public static function isStart($status): bool
    {
        return \in_array($status, [
            self::STATUS_NEW,
            self::STATUS_ERROR,
        ], true);
    }

    public static function getAlias($type = null)
    {
        $aliases = [
            'confirm' => 'осн',
            'expansion' => 'расш',
            'reduction' => 'сокр',
        ];

        return $type === null ? $aliases : $aliases[$type];
    }

}