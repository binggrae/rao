<?php


namespace core\helpers;


use core\entities\AddFileHistory;
use core\entities\StatusHistory;
use core\entities\UpdateFileHistory;

class HistoryHelper
{


    public static function getLabels()
    {
        return [
            AddFileHistory::TYPE => 'Добавление файла',
            UpdateFileHistory::TYPE => 'Изменение файла',
            StatusHistory::TYPE => 'Изменение статуса'
        ];
    }

    public static function getLabel($type)
    {
        return self::getLabels()[$type];
    }

}