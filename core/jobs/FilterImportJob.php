<?php


namespace core\jobs;


use core\repositories\CertificateRepository;
use core\services\importer\filter\Importer;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class FilterImportJob extends BaseObject implements JobInterface
{

    public $file;

    /** @var CertificateRepository */
    private $certificates;

    public function execute($queue)
    {
        $this->certificates = \Yii::$container->get(CertificateRepository::class);

        $importer = new Importer($this->file);
        foreach ($importer->getRows() as $row) {
            $certificate = $this->certificates->getByUid($row->id);
            if ($certificate) {
                $certificate->regions = $row->regions;
                $certificate->areas = $row->areas;
                $this->certificates->save($certificate);
            }
        }

        $importer->clear();
    }

}