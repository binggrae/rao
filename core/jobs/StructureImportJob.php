<?php


namespace core\jobs;


use core\entities\Structure;
use core\repositories\CertificateRepository;
use core\services\importer\structure\Importer;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class StructureImportJob extends BaseObject implements JobInterface
{

    public $file;

    /** @var CertificateRepository */
    private $certificates;

    public function execute($queue)
    {
        $this->certificates = \Yii::$container->get(CertificateRepository::class);

        $importer = new Importer($this->file);
        foreach ($importer->getRows() as $row) {
            try {
                var_dump($row['reg_number']);
                $certificate = $this->certificates->getByUid($row['reg_number']);
                if ($certificate) {
                    $structure = $certificate->structure ?: new Structure($row);
                    $structure->certificate_id = $certificate->id;
                    $structure->save();
                }
            } catch (\Exception $e) {
                var_dump($e->getMessage());
            }
        }

        $importer->clear();
    }

}