<?php


namespace console\controllers;


use core\entities\Certificate;
use core\services\parser\Api;
use core\services\parser\jobs\LoadJob;
use PHPExcelReader\SpreadsheetReader;
use thiagoalessio\TesseractOCR\TesseractOCR;
use yii\console\Controller;
use yii\helpers\VarDumper;

class ParserController extends Controller
{

    /** @var Api */
    private $api;

    public function __construct(string $id, $module, Api $api, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->api = $api;
    }

    public function actionRun()
    {
        foreach ($this->api->search() as $ids) {
            \Yii::$app->queue->push(new LoadJob([
                'ids' => $ids,
            ]));
        }
    }

    public function actionTest()
    {
        $certificate = Certificate::find()->where(['id' => 27985])->all();
        $this->api->load($certificate);

    }

    public function actionXls()
    {
        $path = \Yii::getAlias('@console/runtime/data2.xlsx');
        $reader = new \PHPExcel_Reader_Excel2007();
        $reader->setReadDataOnly(true);

        $excel = $reader->load($path);
        $sheet = $excel->getActiveSheet();

        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $highestColumnIndex = \PHPExcel_Cell::columnIndexFromString($highestColumn);

        $is_table = false;
        $is_head = false;
        $values = ['', '', '', '', '', '', ''];
        $result = [];
        for ($row = 1; $row < $highestRow; ++$row) {
            $count = 0;
            for ($col = 0; $col <= $highestColumnIndex; ++$col) {
                $value = $sheet->getCellByColumnAndRow($col, $row)->getValue();
                if ($value) {
                    $count++;
                }
            }
            var_dump($row . ': ' . $count);
            if ($count == 7 && !$is_head) {
                $is_head = true;
                var_dump('head');
                continue;
            }

            if ($count >= 3 && !$is_table) {
                var_dump('table open');
                $is_table = true;
                continue;
            }
            if ($count == 0 && $is_table) {
                var_dump('table close');
                $is_table = false;
            }

            if ($is_table) {
                $temp = [];
                for ($col = 0; $col < $highestColumnIndex; ++$col) {
                    $value = $sheet->getCellByColumnAndRow($col, $row)->getValue();

                    $values[$col] = $value ?: $values[$col];
                    $temp = $values;

                }
                $result[] = $temp;
            }

        }

        file_put_contents(\Yii::getAlias('@console/runtime/result2.txt'), VarDumper::dumpAsString($result));
        var_dump($result);


//        var_dump($data->get);
    }


}