<?php


namespace console\controllers;


use core\entities\Certificate;
use core\helpers\CertificateHelper;
use core\services\parser\Api;
use yii\console\Controller;

class TestController extends Controller
{
    /**
     * @var Api
     */
    private $api;

    private $ids;


    public function actionParse()
    {
        $this->api = \Yii::$container->get(Api::class);

        $certificates = Certificate::find()->where(['job' => 10])->all();
//        $ids = [];
//        foreach ($model as $item) {
//            $ids[] = $item->id;
//        }


//        $certificates = Certificate::find()->byId($ids)->all();

        foreach ($certificates as $certificate) {
            $certificate->job = CertificateHelper::STATUS_PROGRESS;
            $certificate->save();
        }

        $this->api->init($certificates);
    }



}