<?php

use core\helpers\TextTypesTrait;
use yii\db\Migration;

/**
 * Class m180421_173852_fix
 */
class m180421_173852_fix extends Migration
{
    use TextTypesTrait;
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('structures', 'obl_akk_al', $this->text());

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180421_173852_fix cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180421_173852_fix cannot be reverted.\n";

        return false;
    }
    */
}
