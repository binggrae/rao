<?php

use yii\db\Migration;

/**
 * Class m180323_040501_ogrn
 */
class m180323_040501_ogrn extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn('structures', 'ogrn_ap', 'ogrn');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180323_040501_ogrn cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180323_040501_ogrn cannot be reverted.\n";

        return false;
    }
    */
}
