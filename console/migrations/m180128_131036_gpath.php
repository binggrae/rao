<?php

use yii\db\Migration;

/**
 * Class m180128_131036_gpath
 */
class m180128_131036_gpath extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn('file', 'gpath', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180128_131036_gpath cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180128_131036_gpath cannot be reverted.\n";

        return false;
    }
    */
}
