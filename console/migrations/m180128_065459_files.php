<?php

use yii\db\Migration;

/**
 * Class m180128_065459_files
 */
class m180128_065459_files extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('file', [
            'id' => $this->primaryKey(),
            'cert_id' => $this->integer()->null(),
            'uid' => $this->string()->null(),
            'name' => $this->string()->null(),
            'extension' => $this->string()->null(),
            'created_at' => $this->integer(),
            'size' => $this->integer(),
            'status' => $this->integer(),
            'gpath' => $this->integer()
        ], $tableOptions);

        $this->addForeignKey('fk_file_certificate',
            'file', 'cert_id',
            'certificates', 'id',
            'CASCADE', 'RESTRICT'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180128_065459_files cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180128_065459_files cannot be reverted.\n";

        return false;
    }
    */
}
