<?php

use yii\db\Migration;

/**
 * Class m180128_072333_type_files
 */
class m180128_072333_type_files extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('file', 'type', $this->string()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180128_072333_type_files cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180128_072333_type_files cannot be reverted.\n";

        return false;
    }
    */
}
