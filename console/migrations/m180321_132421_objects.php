<?php

use yii\db\Migration;

/**
 * Class m180321_132421_objects
 */
class m180321_132421_objects extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('objects', [
            'id' => $this->primaryKey(),
            'alias' => $this->string()->notNull(),
            'title' => $this->string()->notNull(),
        ], $tableOptions);

        $this->createTable('certificate_objects', [
            'certificate_id' => $this->integer(),
            'object_id' => $this->integer(),
        ], $tableOptions);


        $this->addForeignKey('fk_certificateObject_object',
            'certificate_objects', 'object_id',
            'objects', 'id',
            'CASCADE', 'RESTRICT');

        $this->addForeignKey('fk_certificateObject_certificate',
            'certificate_objects', 'certificate_id',
            'certificates', 'id',
            'CASCADE', 'RESTRICT');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180321_132421_objects cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180321_132421_objects cannot be reverted.\n";

        return false;
    }
    */
}
