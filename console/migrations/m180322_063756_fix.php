<?php

use yii\db\Migration;

/**
 * Class m180322_063756_fix
 */
class m180322_063756_fix extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('structures', 'id', $this->primaryKey()->first());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180322_063756_fix cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180322_063756_fix cannot be reverted.\n";

        return false;
    }
    */
}
