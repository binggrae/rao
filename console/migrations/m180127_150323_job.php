<?php

use yii\db\Migration;

/**
 * Class m180127_150323_job
 */
class m180127_150323_job extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('certificates', 'job', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180127_150323_job cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180127_150323_job cannot be reverted.\n";

        return false;
    }
    */
}
