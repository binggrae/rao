<?php

use yii\db\Migration;

/**
 * Class m180321_112728_regions
 */
class m180321_112728_regions extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('regions', [
            'id' => $this->primaryKey(),
            'alias' => $this->string()->notNull(),
            'title' => $this->string()->notNull(),
        ], $tableOptions);

        $this->addColumn('certificates', 'region_id', $this->integer());

        $this->addForeignKey('fk_certificate_region',
            'certificates', 'region_id',
            'regions', 'id',
            'SET NULL', 'RESTRICT');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180321_112728_regions cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180321_112728_regions cannot be reverted.\n";

        return false;
    }
    */
}
