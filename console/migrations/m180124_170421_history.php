<?php

use yii\db\Migration;

/**
 * Class m180124_170421_history
 */
class m180124_170421_history extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('histories', [
            'id' => $this->primaryKey(),
            'cert_id' => $this->integer()->notNull(),
            'type' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'data' => $this->text(),
        ]);

        $this->addForeignKey('fk_certificate_history',
            'histories', 'cert_id',
            'certificates', 'id',
            'CASCADE', 'RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180124_170421_history cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180124_170421_history cannot be reverted.\n";

        return false;
    }
    */
}
