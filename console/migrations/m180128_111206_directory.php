<?php

use yii\db\Migration;

/**
 * Class m180128_111206_directory
 */
class m180128_111206_directory extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('directory', [
            'id' => $this->primaryKey(),
            'uid' => $this->string()->notNull(),
            'path' => $this->string()->notNull(),
        ], $tableOptions);

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180128_111206_directory cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180128_111206_directory cannot be reverted.\n";

        return false;
    }
    */
}
