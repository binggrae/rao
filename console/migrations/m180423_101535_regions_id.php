<?php

use core\entities\Certificate;
use yii\db\Migration;
use yii\db\Query;

/**
 * Class m180423_101535_regions_id
 */
class m180423_101535_regions_id extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('certificate_region', [
            'certificate_id' => $this->integer(),
            'region_id' => $this->integer(),
        ]);


        $this->addForeignKey('fk_certificateRegion_region',
            'certificate_region', 'region_id',
            'regions', 'id',
            'CASCADE', 'RESTRICT');

        $this->addForeignKey('fk_certificateRegion_certificate',
            'certificate_region', 'certificate_id',
            'certificates', 'id',
            'CASCADE', 'RESTRICT');

        $certificates = Certificate::find()->where(['is not', 'region_id', null])->all();
        foreach ($certificates as $certificate) {
            $this->insert('certificate_region', [
                'certificate_id' => $certificate->id,
                'region_id' => $certificate->region_id,
            ]);
        }

        $this->dropForeignKey('fk_certificate_region', 'certificates');
        $this->dropColumn('certificates', 'region_id');

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180423_101535_regions_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180423_101535_regions_id cannot be reverted.\n";

        return false;
    }
    */
}
