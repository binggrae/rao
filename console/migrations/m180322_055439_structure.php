<?php

use yii\db\Migration;

/**
 * Class m180322_055439_structure
 */
class m180322_055439_structure extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('structures', [
            'certificate_id' => $this->integer(),
            "type_al" => $this->text(),
            "type_ap" => $this->text(),
            "opf_ap" => $this->text(),
            "full_name_ap" => $this->text(),
            "short_name_ap" => $this->text(),
            "trade_name_ap" => $this->text(),
            "fio_ruk_ap" => $this->text(),
            "dol_ruk_ap" => $this->text(),
            "address_ap" => $this->text(),
            "address_delo_ap" => $this->text(),
            "phone_ap" => $this->text(),
            "email_ap" => $this->text(),
            "ogrn_ap" => $this->text(),
            "inn_ap" => $this->text(),
            "full_name_al" => $this->text(),
            "short_name_al" => $this->text(),
            "fio_ruk_al" => $this->text(),
            "address_al" => $this->text(),
            "phone_al" => $this->text(),
            "email_al" => $this->text(),
            "website_al" => $this->text(),
            "lab_al" => $this->text(),
            "own_lab_al" => $this->text(),
            "expert_al" => $this->text(),
            "fio_rab_al" => $this->text(),
            "obl_akk_al" => $this->text(),
            "status" => $this->text(),
            "reg_number" => $this->text(),
            "on_reg_dat" => $this->text(),
            "off_att_akk_dat" => $this->text(),
            "akkred_order_num" => $this->text(),
            "akkred_order_dat" => $this->text(),
            "akkred_exp" => $this->text(),
            "izm_att_akk" => $this->text(),
            "ts" => $this->text(),
            "gk" => $this->text(),
        ], $tableOptions);

        $this->addForeignKey('fk_structure_certificate',
            'structures', 'certificate_id',
            'certificates', 'id',
            'CASCADE', 'RESTRICT');


    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m180322_055439_structure cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180322_055439_structure cannot be reverted.\n";

        return false;
    }
    */
}
