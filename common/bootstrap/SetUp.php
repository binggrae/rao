<?php

namespace common\bootstrap;


use yii\base\BootstrapInterface;
use yii\di\Container;
use yii\di\Instance;
use yii\httpclient\Client;
use yii\queue\Queue;

class SetUp implements BootstrapInterface
{

    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        $container = \Yii::$container;

        $container->set(Client::class, function() use ($app) {
            return  new Client([
                'transport' => 'yii\httpclient\CurlTransport',
            ]);
        });

    }


}