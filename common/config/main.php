<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => [
        'common\bootstrap\SetUp',
        'queue'
    ],
    'language' => 'ru-RU',
    'timeZone' => 'Europe/Moscow',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => '@common/runtime/cache',
        ],
        'queue' => [
            'class' => \yii\queue\amqp_interop\Queue::class,
            'port' => 5672,
            'user' => 'guest',
            'password' => 'guest',
            'queueName' => 'queue',
            'driver' => yii\queue\amqp_interop\Queue::ENQUEUE_AMQP_LIB,
            // // или
            // 'dsn' => 'amqp://guest:guest@localhost:5672/%2F',
            // или
            'dsn' => 'amqp:',
        ],
        'gdrive' => [
            'class' => 'lhs\Yii2FlysystemGoogleDrive\GoogleDriveFilesystem',
            'clientId' => '631568900012-i2sc4d0l49akhv00ge1qqopa480fv5a4.apps.googleusercontent.com',
            'clientSecret' => 'KW_prz8_m6Y87hvqADXstiD7',
            'refreshToken' => '1/KTj0oKwsCGNtagupaBM1xDH-bZ-9oZOniMfMwSyKmrI',
            'rootFolderId' => '1qdPevGpzqzEu5lwDueVWF5-sRHSOX2jB'
        ],
    ],
];
